drop view S16F5_Employee_view;
drop view S16F5_Prev_Employee_view;
drop view S16F5_Project_Employee_view;
drop view S16F5_Manager_view;
drop view S16F5_President_view;
drop view S16F5_Interim_Manager_view;
drop view S16F5_Current_Project_view;
drop view S16F5_Previous_Project_view;

create view S16F5_Project_Employee_view as 
SELECT 
    person_id,
    type,
    first_name,
    last_name,
    home_address,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    spouse_person_id
FROM S16F5_Person where type = 'Project_Employee' ;

create or replace TRIGGER S16F5_Project_Employee_trigger
     INSTEAD OF insert ON S16F5_Project_Employee_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Person( 
        person_id,
        type,
        first_name,
        last_name,
        home_address,
        zipcode,
        home_phone,
        us_citizen,
        employee_id,
        salary,
        salary_exception,
        spouse_person_id)
    VALUES ( 
        :NEW.person_id,
        'Project_Employee',
        :NEW.first_name,
        :NEW.last_name,
        :NEW.home_address,
        :NEW.zipcode,
        :NEW.home_phone,
        :NEW.us_citizen,
        :NEW.employee_id,
        :NEW.salary,
        :NEW.salary_exception,
        :NEW.spouse_person_id) ;
END;
/

create view S16F5_Manager_view as 
SELECT 
    bonus,
    person_id,
    type,
    first_name,
    last_name,
    home_address,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    dept_no,
    spouse_person_id
FROM S16F5_Person where type = 'Manager' ;

create or replace TRIGGER S16F5_Manager_trigger
     INSTEAD OF insert ON S16F5_Manager_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Person( 
        bonus,
        person_id,
        type,
        first_name,
        last_name,
        home_address,
        zipcode,
        home_phone,
        us_citizen,
        employee_id,
        salary,
        salary_exception,
        dept_no,
        spouse_person_id)
    VALUES ( 
        :NEW.bonus,
        :NEW.person_id,
        'Manager',
        :NEW.first_name,
        :NEW.last_name,
        :NEW.home_address,
        :NEW.zipcode,
        :NEW.home_phone,
        :NEW.us_citizen,
        :NEW.employee_id,
        :NEW.salary,
        :NEW.salary_exception,
        :NEW.dept_no,
        :NEW.spouse_person_id) ;
END;
/

create view S16F5_Employee_view as 
SELECT 
    person_id,
    type,
    first_name,
    last_name,
    home_address,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    manager_person_id,
    spouse_person_id
FROM S16F5_Person where type = 'Employee' ;

create or replace TRIGGER S16F5_Employee_trigger
     INSTEAD OF insert ON S16F5_Employee_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Person( 
        person_id,
        type,
        first_name,
        last_name,
        home_address,
        zipcode,
        home_phone,
        us_citizen,
        employee_id,
        salary,
        salary_exception,
        manager_person_id,
        spouse_person_id)
    VALUES ( 
        :NEW.person_id,
        'Employee',
        :NEW.first_name,
        :NEW.last_name,
        :NEW.home_address,
        :NEW.zipcode,
        :NEW.home_phone,
        :NEW.us_citizen,
        :NEW.employee_id,
        :NEW.salary,
        :NEW.salary_exception,
        :NEW.manager_person_id,
        :NEW.spouse_person_id) ;
END;
/

create view S16F5_Prev_Employee_view as 
SELECT 
    person_id,
    type,
    first_name,
    last_name,
    home_address,
    zipcode,
    home_phone,
    us_citizen,
    isFired,
    salary,
    spouse_person_id
FROM S16F5_Person where type = 'Previous_Employee' ;

create or replace TRIGGER S16F5_Prev_Employee_trigger
     INSTEAD OF insert ON S16F5_Prev_Employee_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Person( 
        person_id,
        type,
        first_name,
        last_name,
        home_address,
        zipcode,
        home_phone,
        us_citizen,
        isFired,
        salary,
        spouse_person_id)
    VALUES ( 
        :NEW.person_id,
        'Previous_Employee',
        :NEW.first_name,
        :NEW.last_name,
        :NEW.home_address,
        :NEW.zipcode,
        :NEW.home_phone,
        :NEW.us_citizen,
        :NEW.isFired,
        :NEW.salary,
        :NEW.spouse_person_id) ;
END;
/

create view S16F5_President_view as
SELECT 
    person_id,
    type,
    first_name,
    last_name,
    home_address,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    bonus,
    dept_no,
    spouse_person_id
FROM S16F5_Person where type = 'President' ;

create or replace TRIGGER S16F5_President_trigger
     INSTEAD OF insert ON S16F5_President_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Person( 
        person_id,
        type,
        first_name,
        last_name,
        home_address,
        zipcode,
        home_phone,
        us_citizen,
        employee_id,
        salary,
        salary_exception,
        bonus,
        dept_no,
        spouse_person_id)
    VALUES ( 
        :NEW.person_id,
        'President',
        :NEW.first_name,
        :NEW.last_name,
        :NEW.home_address,
        :NEW.zipcode,
        :NEW.home_phone,
        :NEW.us_citizen,
        :NEW.employee_id,
        :NEW.salary,
        :NEW.salary_exception,
        :NEW.bonus,
        :NEW.dept_no,
        :NEW.spouse_person_id) ;
END;
/

create view S16F5_Interim_Manager_view as
SELECT 
    person_id,
    type,
    first_name,
    last_name,
    home_address,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    bonus,
    dept_no,
    spouse_person_id
FROM S16F5_Person where type = 'Interim_Manager' ;

create or replace TRIGGER S16F5_Interim_Manager_trigger
     INSTEAD OF insert ON S16F5_Interim_Manager_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Person( 
        person_id,
        type,
        first_name,
        last_name,
        home_address,
        zipcode,
        home_phone,
        us_citizen,
        employee_id,
        salary,
        salary_exception,
        bonus,
        dept_no,
        spouse_person_id)
    VALUES ( 
        :NEW.person_id,
        'Interim_Manager',
        :NEW.first_name,
        :NEW.last_name,
        :NEW.home_address,
        :NEW.zipcode,
        :NEW.home_phone,
        :NEW.us_citizen,
        :NEW.employee_id,
        :NEW.salary,
        :NEW.salary_exception,
        :NEW.bonus,
        :NEW.dept_no,
        :NEW.spouse_person_id) ;
END;
/

create view S16F5_Current_Project_view as
SELECT 
    project_no,
    type,
    project_title,
    project_active,
    approved,
    dept_no,
    parent_project_no,
    manager_person_id
FROM S16F5_Project where type = 'Current_Project' ;

create or replace TRIGGER S16F5_Current_Project_trigger
     INSTEAD OF insert ON S16F5_Current_Project_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Project( 
        project_no,
        type,
        project_title,
        project_active,
        approved,
        dept_no,
        parent_project_no,
        manager_person_id)
    VALUES ( 
        :NEW.project_no,
        'Current_Project',
        :NEW.project_title,
        :NEW.project_active,
        :NEW.approved,
        :NEW.dept_no,
        :NEW.parent_project_no,
        :NEW.manager_person_id) ;
END;
/

create view S16F5_Previous_Project_view as
SELECT 
    project_no,
    type,
    project_title,
    end_date_month,
    end_date_day,
    end_date_year,
    est_person_hours,
    approved,
    dept_no,
    parent_project_no,
    manager_person_id
FROM S16F5_Project where type = 'Previous_Project' ;

create or replace TRIGGER S16F5_Previous_Project_trigger
     INSTEAD OF insert ON S16F5_Previous_Project_view
     FOR EACH ROW
BEGIN
    insert into S16F5_Project( 
        project_no,
        type,
        project_title,
        end_date_month,
        end_date_day,
        end_date_year,
        est_person_hours,
        approved,
        dept_no,
        parent_project_no,
        manager_person_id)
    VALUES ( 
        :NEW.project_no,
        'Previous_Project',
        :NEW.project_title,
        :NEW.end_date_month,
        :NEW.end_date_day,
        :NEW.end_date_year,
        :NEW.est_person_hours,
        :NEW.approved,
        :NEW.dept_no,
        :NEW.parent_project_no,
        :NEW.manager_person_id) ;
END;
/
