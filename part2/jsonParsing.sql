-- Employees

select    
jt1.first_name first_name,    
jt2.last_name last_name,    
jt3.person_id person_id,    
jt4.home_address home_address,    
jt5.zipcode zipcode,    
jt6.home_phone home_phone,    
jt7.us_citizen us_citizen,    
jt8.employee_id employee_id,    
jt9.salary salary,    
jt10.salary_exception salary_exception
from apex_collections t,  
json_table(t.clob001, '$.first_name[*]' COLUMNS rid for ordinality, first_name  varchar path  '$')  jt1,  
json_table(t.clob001, '$.last_name[*]'  COLUMNS rid for ordinality, last_name varchar path  '$')  jt2,  
json_table(t.clob001, '$.person_id[*]'  COLUMNS rid for ordinality, person_id varchar path  '$')  jt3,  
json_table(t.clob001, '$.home_address[*]' COLUMNS rid for ordinality, home_address varchar  path  '$')  jt4,  
json_table(t.clob001, '$.zipcode[*]'  COLUMNS rid for ordinality, zipcode varchar path  '$')  jt5,  
json_table(t.clob001, '$.home_phone[*]' COLUMNS rid for ordinality, home_phone varchar  path  '$')  jt6,  
json_table(t.clob001, '$.us_citizen[*]' COLUMNS rid for ordinality, us_citizen varchar  path  '$')  jt7,  
json_table(t.clob001, '$.employee_id[*]'  COLUMNS rid for ordinality, employee_id varchar path  '$')  jt8,  
json_table(t.clob001, '$.salary[*]' COLUMNS rid for ordinality, salary  varchar path  '$')  jt9,  
json_table(t.clob001, '$.salary_exception[*]' COLUMNS rid for ordinality, salary_exception varchar  path  '$')  jt10  
where t.collection_name = 'P2_EMPLOYEE_JSON'and   
jt1.rid = jt2.rid and jt2.rid = jt3.rid and jt3.rid = jt4.rid and jt4.rid = jt5.rid and jt5.rid = jt6.rid and jt6.rid = jt7.rid and 
jt7.rid = jt8.rid and jt8.rid = jt9.rid and jt9.rid = jt10.rid  


