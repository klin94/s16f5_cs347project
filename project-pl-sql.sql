-- PL/SQL Code:

-- Page 1:

-- SELECT_PERSON item:

SELECT first_name || ' ' || last_name AS display_value, person_id AS return_value 
FROM S16F5_Person
WHERE type = :P1_SELECT_TYPE;


-- Page 4

-- SET_APPROVE Process

BEGIN
  UPDATE S16F5_Project 
  SET approved = 1
  WHERE project_no = :P4_PROJECT_NO;
END;


-- Page 6

-- P6_SPOUSE_PERSON_ID

select first_name || ' ' || last_name as display_value,
person_id as return_value
from S16F5_Person
where spouse_person_id is NULL;


-- LINK_SPOUSE Process

update S16F5_Person
set spouse_person_id = :P6_PERSON_ID
where person_id = :P6_SPOUSE_PERSON_ID;


-- Page 7

-- SET_APPROVE Process

BEGIN
IF :P1_SELECT_TYPE = 'President' THEN
    :P7_APPROVED := 1;
END IF;
END;


-- Page 12

-- P12_SPOUSE_PERSON_ID

select first_name || ' ' || last_name as display_value,
person_id as return_value
from S16F5_Person
where spouse_person_id is NULL and person_id <> :SELECT_PERSON;


-- Page 13

-- P13_SPOUSE_PERSON_ID

select first_name || ' ' || last_name as display_value,
person_id as return_value
from S16F5_Person
where spouse_person_id is NULL or person_id = :P13_SPOUSE_PERSON_ID;


-- Page 15

-- P15_SPOUSE_PERSON_ID

select first_name || ' ' || last_name as display_value,
person_id as return_value
from S16F5_Person
where spouse_person_id is NULL;


-- Page 19

-- P19_PERSON_ID

SELECT P.first_name || ' ' || P.last_name AS display_value, P.person_id AS return_value
FROM S16F5_Person P 
WHERE P.person_id NOT IN 
(SELECT PC.person_id FROM S16F5_PEmp_CurrP PC WHERE PC.project_no = :P19_PROJECT_NO) 
AND (P.type = 'Employee' OR P.type = 'Project_Employee');


-- ASSIGN_EMP Process

UPDATE S16F5_PERSON
SET type='Project_Employee'
WHERE person_id = :P19_PERSON_ID;


-- Page 21

-- P21_SELECT_EMP

select first_name || ' ' || last_name as display_value, person_id as return_value 
from S16F5_Person 
where type = 'Employee' or type = 'Project_Employee' or type = 'Manager' or type = 'Interim_Manager';


-- UPDATE_EMP

BEGIN
update S16F5_Person
set bonus=null, employee_id=null, salary_exception=null, dept_no=null, manager_person_id=null,
isfired=:P21_ISFIRED, type='Previous_Employee'
where person_id = :P21_SELECT_EMP;
END;


-- Page 22

-- P22_CHILD_PERSON

select first_name || ' ' || last_name as display_value, person_id as return_value 
from S16F5_Person 
where person_id <> :SELECT_PERSON;


-- P22_SPOUSE

select first_name || ' ' || last_name as display_value,
person_id as return_value
from S16F5_Person
where spouse_person_id is NULL and person_id <> :SELECT_PERSON;


-- ADD_FAM_MEM

BEGIN
IF :P22_SELECT_MEMBER = 0 THEN
  update S16F5_Person
  set spouse_person_id=:P22_SPOUSE
  where person_id = :SELECT_PERSON;
      
  update S16F5_Person
  set spouse_person_id=:SELECT_PERSON
  where person_id = :P22_SPOUSE;
END IF;
IF :P22_SELECT_MEMBER = 1 THEN
  INSERT INTO S16F5_Child_Parent VALUES(:SELECT_PERSON, :P22_CHILD_PERSON);
END IF;
END;


-- Page 26

-- P26_FIRST_NAME

SELECT first_name
FROM S16F5_PERSON
WHERE person_id = :SELECT_PERSON;


-- P26_LAST_NAME

SELECT last_name
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- P26_EMPLOYEE_ID

SELECT employee_id 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- P26_HOME_ADDRESS

SELECT home_address 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- P26_ZIPCODE

SELECT zipcode 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- P26_HOME_PHONE

SELECT home_phone
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- P26_US_CITIZEN

SELECT 'False' 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON AND us_citizen = 0
UNION
SELECT 'True' 
FROM S16F5_PERSON 
wHERE person_id = :SELECT_PERSON AND us_citizen = 1


-- P26_SPOUSE

DECLARE
spouse integer;
result VARCHAR(255);
BEGIN
  SELECT spouse_person_id into spouse 
  FROM S16F5_PERSON 
  WHERE person_id = :SELECT_PERSON;
  IF spouse is NULL THEN
    return '';
  END IF;
SELECT first_name || ' ' || last_name into result 
FROM S16F5_PERSON 
WHERE person_id = spouse;
return result;
END;


-- P26_SALARY

SELECT salary 
FROM S16F5_PERSON
WHERE person_id = :SELECT_PERSON;


-- P26_SALARY_EXCEPTION

SELECT 'False' 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON AND SALARY_EXCEPTION = 0
UNION
SELECT 'True' 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON AND SALARY_EXCEPTION = 1


-- P26_BONUS

SELECT bonus 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- P26_MANAGER_PERSON_ID

DECLARE
manager integer;
result VARCHAR(255);
BEGIN
  SELECT manager_person_id into manager FROM S16F5_PERSON WHERE person_id = :SELECT_PERSON;
  IF manager is NULL THEN
    return '';
  END IF;
  SELECT first_name || ' ' || last_name into result 
  FROM S16F5_PERSON 
  WHERE person_id = manager;
  return result;
END;


-- P26_DEPT_NO

SELECT dept_no 
FROM S16F5_PERSON 
WHERE person_id = :SELECT_PERSON;


-- Previous Projects Report

SELECT
  P.project_no, 
  P.project_title, 
  P.project_active,
  P.approved
  FROM S16F5_Project P JOIN S16F5_PEmp_CurrP PC ON 
  P.project_no = PC.project_no
  WHERE PC.person_id = :SELECT_PERSON AND P.type = 'Previous_Project'
UNION
SELECT
  PO.project_no,
  PO.project_title,
  PO.project_active,
  PO.approved
  FROM S16F5_Project PO JOIN S16F5_Person PE ON
  PO.manager_person_id = PE.person_id
  WHERE PE.person_id = :SELECT_PERSON AND PO.type = 'Previous_Project'


-- Current Projects Report


SELECT
  P.project_no, 
  P.project_title, 
  P.project_active,
  P.approved
  FROM S16F5_Project P JOIN S16F5_PEmp_CurrP PC ON 
  P.project_no = PC.project_no
  WHERE PC.person_id = :SELECT_PERSON AND P.type = 'Current_Project'
UNION
SELECT
  PO.project_no,
  PO.project_title,
  PO.project_active,
  PO.approved
  FROM S16F5_Project PO JOIN S16F5_Person PE ON
  PO.manager_person_id = PE.person_id
  WHERE PE.person_id = :SELECT_PERSON and PO.type = 'Current_Project'



