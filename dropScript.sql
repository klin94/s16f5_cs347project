DROP TABLE Child_Parent CASCADE CONSTRAINTS ;
DROP TABLE Department CASCADE CONSTRAINTS ;
DROP TABLE Person CASCADE CONSTRAINTS ;
DROP TABLE ProjEmp_CurrProj CASCADE CONSTRAINTS ;
DROP TABLE Project CASCADE CONSTRAINTS ;
DROP SEQUENCE Department_seq ; 
DROP TRIGGER Department_PK_trig ;
DROP SEQUENCE Person_seq ; 
DROP TRIGGER Person_PK_trig ;
DROP SEQUENCE Project_seq ; 
DROP TRIGGER Project_PK_trig ;
DROP INDEX parent_person_id_FK_0 ;
DROP INDEX child_person_id_FK_1 ;
DROP INDEX dept_no_FK_2 ;
DROP INDEX manager_person_id_FK_3 ;
DROP INDEX spouse_person_id_FK_4 ;
DROP INDEX person_id_FK_5 ;
DROP INDEX project_no_FK_6 ;
DROP INDEX dept_no_FK_7 ;
DROP INDEX manager_person_id_FK_8 ;
DROP INDEX parent_project_no_FK_9 ;
DROP SEQUENCE Department_seq ; 
DROP SEQUENCE Person_seq ; 
DROP SEQUENCE Project_seq ; 
DROP INDEX parent_person_id_FK_0 ;
DROP INDEX child_person_id_FK_1 ;
DROP INDEX dept_no_FK_2 ;
DROP INDEX manager_person_id_FK_3 ;
DROP INDEX spouse_person_id_FK_4 ;
DROP INDEX person_id_FK_5 ;
DROP INDEX project_no_FK_6 ;
DROP INDEX dept_no_FK_7 ;
DROP INDEX manager_person_id_FK_8 ;
DROP INDEX parent_project_no_FK_9 ;


DROP TABLE F16F5_Child_Parent CASCADE CONSTRAINTS ;
DROP TABLE F16F5_Department CASCADE CONSTRAINTS ;
DROP TABLE F16F5_PEmp_CurrP CASCADE CONSTRAINTS ;
DROP TABLE F16F5_Person CASCADE CONSTRAINTS ;
DROP TABLE F16F5_Project CASCADE CONSTRAINTS ;
DROP SEQUENCE F16F5_Department_seq ; 
DROP TRIGGER F16F5_Department_PK_trig 
DROP SEQUENCE F16F5_Person_seq ; 
DROP TRIGGER F16F5_Person_PK_trig 
DROP SEQUENCE F16F5_Project_seq ; 
DROP TRIGGER F16F5_Project_PK_trig 
DROP TABLE S16F5_Child_Parent CASCADE CONSTRAINTS ;
DROP TABLE S16F5_Department CASCADE CONSTRAINTS ;
DROP TABLE S16F5_PEmp_CurrP CASCADE CONSTRAINTS ;
DROP TABLE S16F5_Person CASCADE CONSTRAINTS ;
DROP TABLE S16F5_Project CASCADE CONSTRAINTS ;
DROP SEQUENCE S16F5_Department_seq ; 
DROP TRIGGER S16F5_Department_PK_trig 
DROP SEQUENCE S16F5_Person_seq ; 
DROP TRIGGER S16F5_Person_PK_trig 
DROP SEQUENCE S16F5_Project_seq ; 
DROP TRIGGER S16F5_Project_PK_trig 
DROP INDEX child_person_id_FK_0 ;
DROP INDEX dept_no_FK_1 ;
DROP INDEX dept_no_FK_2 ;
DROP INDEX manager_person_id_FK_3 ;
DROP INDEX manager_person_id_FK_4 ;
DROP INDEX parent_person_id_FK_5 ;
DROP INDEX parent_project_no_FK_6 ;
DROP INDEX person_id_FK_7 ;
DROP INDEX project_no_FK_8 ;
DROP INDEX spouse_person_id_FK_9 ;
DROP SEQUENCE S16F5_Department_seq ; 
DROP SEQUENCE S16F5_Person_seq ; 
DROP SEQUENCE S16F5_Project_seq ; 
DROP INDEX child_person_id_FK_0 ;
DROP INDEX dept_no_FK_1 ;
DROP INDEX dept_no_FK_2 ;
DROP INDEX manager_person_id_FK_3 ;
DROP INDEX manager_person_id_FK_4 ;
DROP INDEX parent_person_id_FK_5 ;
DROP INDEX parent_project_no_FK_6 ;
DROP INDEX person_id_FK_7 ;
DROP INDEX project_no_FK_8 ;
DROP INDEX spouse_person_id_FK_9 ;
